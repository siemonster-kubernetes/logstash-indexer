#!/bin/sh

set -e
/run/restore-config.sh

chown -R logstash /usr/share/logstash \
	/etc/listbot

exec "$@"
