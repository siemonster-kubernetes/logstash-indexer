#!/bin/bash
# Copyright SIEMonster 2017
# Maintained by jim@siemonster.com
# Dynamic Logstash intel feed insertion
# IPRep
function fuCLEANUP {
  exit 0
}
trap fuCLEANUP EXIT
# mitre_attack
wget -nc -O /usr/share/logstash/config/mitre_attack.csv https://github.com/siemonster/misc/raw/master/mitre_attack.csv
# Download updated translation maps
cd /etc/listbot 
git pull --all --depth=1
#Minemeld
cd /tmp
cp /usr/share/logstash/pipeline/90-osint-filter.conf .
wget -O minemeldsource http://minemeld/feeds/inboundfeedhc?tr=1
if [ ! -s minemeldsource ]
then
  exit 1
else
  # Convert list to comma delimited
  awk -vORS=, '{ print "\""$0"\"" }' minemeldsource | sed 's/,$/\n/' > cidrs.txt
  read feedupdate < cidrs.txt
  # Insert update
  sed -i "/^\s*network/ s|\[.*\]|\[${feedupdate}\]|g" 90-osint-filter.conf
  rsync -vh 90-osint-filter.conf /usr/share/logstash/pipeline/90-osint-filter.conf
fi
